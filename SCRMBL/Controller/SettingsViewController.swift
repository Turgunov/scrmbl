//
//  SettingsViewController.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 25.07.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var themeChangingView: UIView!
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var navigationBarTitleLabel: UILabel!
    
    //MARK: - ViewControllerLifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(updateTheme), name: NSNotification.Name(rawValue: Observers.themeDidChangeKey), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareTheme()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Utility Functions
    private func prepareTheme() {
        navigationBarView.backgroundColor = ThemeManager.currentTheme().mainColor
        navigationBarTitleLabel.textColor = ThemeManager.currentTheme().titleTextColor
    }
    
    //MARK: - Handlers
    @objc private func updateCopyNotificationView() {
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseIn, animations: {
            self.themeChangingView.alpha = 0.0
        }) { (complete) in
            if complete {
                self.themeChangingView.isHidden = true
            }
        }
    }
    
    @objc private func updateTheme() {
        themeChangingView.backgroundColor = ThemeManager.currentTheme().mainColor
        themeChangingView.isHidden = false
        themeChangingView.alpha = 1.0
        Timer.scheduledTimer(timeInterval: 0, target: self, selector: #selector(updateCopyNotificationView), userInfo: nil, repeats: false)
        
        prepareTheme()
    }

}
