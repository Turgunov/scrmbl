//
//  LevelsTableViewController.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 16.07.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit

class LevelsTableViewController: UITableViewController {

    //MARK: - ViewControllerLifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearsSelectionOnViewWillAppear = true
    }

    //MARK: - UITableViewControllerDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            AppSettings.shared.chosenLevel = "Very Low"
            dismiss(animated: true, completion: nil)
        case 1:
            AppSettings.shared.chosenLevel = "Low"
            dismiss(animated: true, completion: nil)
        case 2:
            AppSettings.shared.chosenLevel = "Medium"
            dismiss(animated: true, completion: nil)
        case 3:
            AppSettings.shared.chosenLevel = "High"
            dismiss(animated: true, completion: nil)
        case 4:
            AppSettings.shared.chosenLevel = "Very High"
            dismiss(animated: true, completion: nil)
        default: break
        }
    }

}
