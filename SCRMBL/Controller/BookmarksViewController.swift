//
//  BookmarksViewController.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 17.07.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit
import CoreData

class BookmarksViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var navigationBarTitleLabel: UILabel!
    @IBOutlet weak var trashButton: UIButton!
    @IBOutlet weak var messageLabel: UIStackView!
    @IBOutlet weak var tableView: UITableView!  {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    
    private var bookmarks: [Bookmark] = []

    //MARK: - ViewControllerLifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareTheme()
        fetchCoreDataObjects()
        tableView.reloadData()
    }
    
    //MARK: - Core Data
    private func fetchCoreDataObjects() {
        fetch { success in
            if success {
                if bookmarks.count > 0 {
                    tableView.isHidden = false
                    messageLabel.isHidden = true
                }else {
                    tableView.isHidden = true
                    messageLabel.isHidden = false
                }
            }
        }
    }
    
    private func fetch(completion: CompletionHandler) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        
        let fetchRequest = NSFetchRequest<Bookmark>(entityName: "Bookmark")
        do {
            let fetchedItems = try managedContext.fetch(fetchRequest)
            var sortedItems: [Bookmark] = []
            
            fetchedItems.forEach { (bookmark) in
                if bookmark.saveStatus {
                    sortedItems.append(bookmark)
                }else {
                    managedContext.delete(bookmark)
                }
            }
            
            bookmarks = sortedItems

            do {
                try managedContext.save()
            }catch {
                debugPrint("Could not remove: \(error.localizedDescription)")
                completion(false)
            }
            completion(true)
        }catch {
            debugPrint("Could not fetch \(error.localizedDescription)")
            completion(false)
        }
    }
    
    //MARK: - Utility Functions
    private func prepareTheme() {
        trashButton.setImage(trashButton.currentImage!.withRenderingMode(.alwaysTemplate), for: .normal)
        trashButton.tintColor = ThemeManager.currentTheme().navigationButtonsColor
        navigationBarView.backgroundColor = ThemeManager.currentTheme().mainColor
        navigationBarTitleLabel.textColor = ThemeManager.currentTheme().titleTextColor
    }
    
}

extension BookmarksViewController: UITableViewDelegate, UITableViewDataSource, BookmarkCellDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookmarks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Cells.BookmarkCellId, for: indexPath) as? BookmarkCell else { return UITableViewCell() }
        
        let bookmark = bookmarks[indexPath.row]
        cell.configureCell(bookmark: bookmark)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }

    private func deleteAllObjects(context: NSManagedObjectContext) {
        let productsRequest = Bookmark.fetchRequest() as NSFetchRequest<NSFetchRequestResult>
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: productsRequest)
        deleteRequest.resultType = .resultTypeCount
        _ = try! context.execute(deleteRequest) as! NSBatchDeleteResult
    }

    private func removeBookmark(atIndexPath indexPath: IndexPath) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        managedContext.delete(bookmarks[indexPath.row])
        do {
            try managedContext.save()
            print("Successfully removed from CoreData")
        }catch {
            debugPrint("Could not remove: \(error.localizedDescription)")
        }
    }
    
    func change(saveStatus status: Bool, atIndexPath indexPath: IndexPath) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        bookmarks[indexPath.row].saveStatus = status
        do {
            try managedContext.save()
            print("Successfully saved.")
        }catch {
            debugPrint("Could not save \(error.localizedDescription)")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? BookmarkCell else { return }
        guard let oldText = cell.oldTextLabel.text else { return }
        guard let scrambledText = cell.scrambledTextLabel.text else { return }
        
        guard let expandBookmarkVC = storyboard?.instantiateViewController(withIdentifier: StoryboardIDs.ExpandBookmarkViewControllerId) as? ExpandBookmarkViewController else { return }
        expandBookmarkVC.initWith(old: oldText, scrambled: scrambledText)
        present(expandBookmarkVC, animated: true, completion: nil)
    }
    
    //MARK: - Actions
    
    @IBAction func deleteAllButtonPressed(_ sender: UIButton) {
        if !bookmarks.isEmpty {
            let alert = UIAlertController(title: "Are you sure?", message: "All saved Bookmarks will be deleted.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: .destructive) { (action) in
                guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
                self.bookmarks.forEach { (bookmark) in
                    managedContext.delete(bookmark)
                }
                do {
                    try managedContext.save()
                    self.bookmarks.removeAll()
                    print("Successfully removed from CoreData")
                    self.fetchCoreDataObjects()
                    self.tableView.reloadData()
                }catch {
                    debugPrint("Could not remove: \(error.localizedDescription)")
                }
            })
            alert.addAction(UIAlertAction(title: "No", style: .default) { (action) in
                print("User declined removal of Bookmarks")
            })
            self.present(alert, animated: true)
        }
    }
    
    func bookmarkCellDidTapStar(_ sender: BookmarkCell) {
        guard let tappedIndexPath = tableView.indexPath(for: sender) else { return }
        print("To be removed - \(tappedIndexPath.row)")
        change(saveStatus: false, atIndexPath: tappedIndexPath)
    }
    
    func bookmarkCellDidTapStarFilled(_ sender: BookmarkCell) {
        guard let tappedIndexPath = tableView.indexPath(for: sender) else { return }
        print("Removal canceled - \(tappedIndexPath.row)")
        change(saveStatus: true, atIndexPath: tappedIndexPath)
    }
    
}
