//
//  SettingsTableViewController.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 16.07.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {

    //MARK: - Outlets
    @IBOutlet var colorSelectionButtons: [CircledButton]!
    @IBOutlet var tapToReturnSwitch: UISwitch!
    @IBOutlet var clipboardScrambleSwitch: UISwitch!
        
    //MARK: - ViewControllerLifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearsSelectionOnViewWillAppear = true
        setUpView()
    }
    
    //MARK: - Utility Functions
    private func setUpView() {
        clipboardScrambleSwitch.isOn = AppSettings.shared.getScrambleFromClipboardSettings()
        tapToReturnSwitch.isOn = AppSettings.shared.getScrambleOnTapReturn()
        
        colorSelectionButtons[AppSettings.shared.getSelectedColorButtonIndex()].selectionStatus = true
        colorSelectionButtons[AppSettings.shared.getSelectedColorButtonIndex()].setNeedsLayout()
    }
    
    private func updateTheme() {
        NotificationCenter.default.post(name: Notification.Name(Observers.themeDidChangeKey), object: nil)
        tableView.reloadData()
    }
    
    //MARK: - Actions
    @IBAction func tapToReturnSwitchChanged(_ sender: UISwitch) {
        if sender.isOn {
            AppSettings.shared.shouldScrambleOnTapReturn = true
        }else {
            AppSettings.shared.shouldScrambleOnTapReturn = false
        }
        print(AppSettings.shared.shouldScrambleOnTapReturn)
    }
    
    @IBAction func clipboardScrambleSwitchChanged(_ sender: UISwitch) {
        if sender.isOn {
            AppSettings.shared.shouldScrambleFromClipboard = true
        }else {
            AppSettings.shared.shouldScrambleFromClipboard = false
        }
    }
    
    @IBAction func colorButtonTapped(_ sender: CircledButton) {
        colorSelectionButtons.forEach { (button) in
            button.selectionStatus = false
            button.setNeedsLayout()
        }
        
        switch sender.tag {
        case 0:
            AppSettings.shared.selectedTheme = "green"
            AppSettings.shared.selectedColorButtonIndex = 0
            ThemeManager.applyTheme(theme: .green)
        case 1:
            AppSettings.shared.selectedTheme = "yellow"
            AppSettings.shared.selectedColorButtonIndex = 1
            ThemeManager.applyTheme(theme: .yellow)
        case 2:
            AppSettings.shared.selectedTheme = "blue"
            AppSettings.shared.selectedColorButtonIndex = 2
            ThemeManager.applyTheme(theme: .blue)
        case 3:
            AppSettings.shared.selectedTheme = "red"
            AppSettings.shared.selectedColorButtonIndex = 3
            ThemeManager.applyTheme(theme: .red)
        case 4:
            AppSettings.shared.selectedTheme = "navy"
            AppSettings.shared.selectedColorButtonIndex = 4
            ThemeManager.applyTheme(theme: .navy)
        case 5:
            AppSettings.shared.selectedTheme = "dark"
            AppSettings.shared.selectedColorButtonIndex = 5
            ThemeManager.applyTheme(theme: .dark)
        default: break
        }
        sender.selectionStatus = true
        updateTheme()
    }
    
}

extension SettingsTableViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 3:
            print("Report a problem cell pressed.")
            guard let reportVC = storyboard?.instantiateViewController(withIdentifier: StoryboardIDs.ReportViewControllerId) as? ReportViewController else { return }
            present(reportVC, animated: true, completion: nil)
        case 4:
            print("Ignore cases cell pressed.")
            guard let reportVC = storyboard?.instantiateViewController(withIdentifier: StoryboardIDs.ConfigurationViewControllerId) as? ConfigurationViewController else { return }
            present(reportVC, animated: true, completion: nil)
        case 5:
            print("Review cell pressed.")
            
            UIApplication.shared.open(URL(string: "http://www.google.com")!, options: [:]) { _ in
                tableView.deselectRow(at: IndexPath(row: 5, section: 0), animated: true)
            }
        case 6:
            print("About App cell pressed.")
            guard let aboutAppVC = storyboard?.instantiateViewController(withIdentifier: StoryboardIDs.AboutAppViewControllerId) as? AboutAppViewController else { return }
            present(aboutAppVC, animated: true, completion: nil)
        default: break
        }
    }
}
