//
//  ExpandBookmarkViewController.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 19.07.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit

class ExpandBookmarkViewController: UIViewController {


    //MARK: - Outlets
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var originalTitleLabel: UILabel!
    @IBOutlet weak var scrambledTitleLabel: UILabel!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var topSeparatorView: UIView!
    @IBOutlet weak var bottomSeparatorView: UIView!
    @IBOutlet weak var originalTextLabel: UILabel!
    @IBOutlet weak var scrambledTextLabel: UILabel!
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var notificationMessageLabel: UILabel!
    
    var originalText: String?
    var scrambledText: String?

    //MARK: - ViewControllerLifecycle
    func initWith(old: String, scrambled: String) {
        originalText = old
        scrambledText = scrambled
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareTheme()
    }
    
    //MARK: - Utility Functions
    private func prepareTheme() {
        closeButton.setImage(#imageLiteral(resourceName: "close").withRenderingMode(.alwaysTemplate), for: .normal)
        closeButton.tintColor = ThemeManager.currentTheme().titleTextColor
        
        topSeparatorView.backgroundColor = ThemeManager.currentTheme().titleTextColor
        bottomSeparatorView.backgroundColor = ThemeManager.currentTheme().titleTextColor
        backgroundView.backgroundColor = ThemeManager.currentTheme().mainColor
        originalTitleLabel.textColor = ThemeManager.currentTheme().titleTextColor
        scrambledTitleLabel.textColor = ThemeManager.currentTheme().titleTextColor
        originalTextLabel.textColor = ThemeManager.currentTheme().titleTextColor
        scrambledTextLabel.textColor = ThemeManager.currentTheme().titleTextColor
        notificationView.backgroundColor = ThemeManager.currentTheme().notificationViewColor
        notificationMessageLabel.textColor = ThemeManager.currentTheme().subtitleTextColor
    }
    
    private func setUpView() {
        originalTextLabel.text = originalText
        scrambledTextLabel.text = scrambledText
        
        let longPressGestureRecognizerForOriginal = UILongPressGestureRecognizer(target: self, action: #selector(longPressForOriginalTextHandler(_:)))
        originalTextLabel.isUserInteractionEnabled = true
        longPressGestureRecognizerForOriginal.minimumPressDuration = 1.0
        
        let longPressGestureRecognizerForScrambled = UILongPressGestureRecognizer(target: self, action: #selector(longPressForScrambledTextHandler(_:)))
        scrambledTextLabel.isUserInteractionEnabled = true
        longPressGestureRecognizerForOriginal.minimumPressDuration = 1.0
        
        originalTextLabel.addGestureRecognizer(longPressGestureRecognizerForOriginal)
        scrambledTextLabel.addGestureRecognizer(longPressGestureRecognizerForScrambled)
    }

    private func popNotification(withText text: String) {
        notificationMessageLabel.textColor = backgroundView.backgroundColor
        notificationMessageLabel.text = text
        notificationView.isHidden = false
        notificationView.alpha = 1.0
        
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCopyNotificationView), userInfo: nil, repeats: false)
    }
    
    //MARK: - Handlers
    @objc private func updateCopyNotificationView() {
        UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseIn, animations: {
            self.notificationView.alpha = 0.0
        }) { (complete) in
            if complete {
                self.notificationView.isHidden = true
            }
        }
    }
    
    @objc func longPressForOriginalTextHandler(_ gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .ended:
            UIPasteboard.general.string = scrambledTextLabel.text
            popNotification(withText: "Text Copied")
        default: break
        }
    }
    
    @objc func longPressForScrambledTextHandler(_ gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .ended:
            UIPasteboard.general.string = scrambledTextLabel.text
            popNotification(withText: "Text Copied")
        default: break
        }
    }
    
    //MARK: - Actions
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}
