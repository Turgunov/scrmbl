//
//  ScrambleViewController.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 14.07.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit
import AVFoundation
import CoreData

class ScrambleViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var navigationBarView: NavigationBarView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contentSeparatorView: UIView!
    @IBOutlet weak var languageButton: RoundedButton!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var outputSpeakerButton: UIButton!
    @IBOutlet weak var bookmarkButton: UIButton!
    @IBOutlet weak var expandButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var copyButton: UIButton!
    @IBOutlet weak var inputTextView: UITextView!
    @IBOutlet weak var outputTextView: UITextView!
    @IBOutlet weak var outputControls: UIStackView!
    @IBOutlet weak var inputSpeakerButton: UIButton!
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var notificationMessageLabel: UILabel!
    @IBOutlet weak var clipboardNotificationView: CopyFromClipboardNotificationView!
    
    private var speechSynthesizer = AVSpeechSynthesizer()
    
    private var chosenLevel: Level?
    
    private var savedTexts = [String]()
    
    //MARK: - ViewControllerLifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        inputTextView.delegate = self
        speechSynthesizer.delegate = self
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareTheme()
        updateView()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Utility Functions
    private func prepareTheme() {
        languageButton.layer.borderWidth = 1
        languageButton.layer.borderColor = ThemeManager.currentTheme().titleTextColor.cgColor
        languageButton.setAttributedTitle(NSAttributedString(string: AppSettings.shared.getChosenLevelName(), attributes: [.foregroundColor : ThemeManager.currentTheme().titleTextColor]), for: .normal)
        navigationBarView.backgroundColor = ThemeManager.currentTheme().mainColor
        notificationView.backgroundColor = ThemeManager.currentTheme().notificationViewColor
        notificationMessageLabel.textColor = ThemeManager.currentTheme().subtitleTextColor
        
        clipboardNotificationView.backgroundColor = ThemeManager.currentTheme().notificationViewColor
        clipboardNotificationView.titleTextLabel.textColor = ThemeManager.currentTheme().subtitleTextColor
        clipboardNotificationView.subtitleTextLabel.textColor = ThemeManager.currentTheme().clipboardNotificationSubtitleColor
        
        languageButton.titleLabel?.textColor = ThemeManager.currentTheme().titleTextColor
        contentView.layer.borderColor = ThemeManager.currentTheme().mainColor.cgColor
        contentSeparatorView.backgroundColor = ThemeManager.currentTheme().mainColor
        inputTextView.inputAccessoryView = nil
        addButtonOnKeyboard(ofTextView: inputTextView, withHeight: 64, title: "SCRMBL", titleColor: ThemeManager.currentTheme().keyboardAccessoryTextColor, backgroundColor: ThemeManager.currentTheme().keyboardAccessoryViewColor, action: #selector(keyboardButtonPressed))
    }
    
    private func updateView() {
        chosenLevel = AppSettings.shared.getChosenLevel()
        languageButton.setTitle(AppSettings.shared.getChosenLevelName(), for: .normal)
    }
    
    private func setUpView() {
        //Setup Buttons
        inputSpeakerButton.setImage(inputSpeakerButton.currentImage?.withRenderingMode(.alwaysTemplate), for: .normal)
        clearButton.setImage(clearButton.currentImage?.withRenderingMode(.alwaysTemplate), for: .normal)
        outputSpeakerButton.setImage(outputSpeakerButton.currentImage?.withRenderingMode(.alwaysTemplate), for: .normal)
        bookmarkButton.setImage(bookmarkButton.currentImage?.withRenderingMode(.alwaysTemplate), for: .normal)
        expandButton.setImage(expandButton.currentImage?.withRenderingMode(.alwaysTemplate), for: .normal)
        shareButton.setImage(shareButton.currentImage?.withRenderingMode(.alwaysTemplate), for: .normal)
        copyButton.setImage(copyButton.currentImage?.withRenderingMode(.alwaysTemplate), for: .normal)
        
        inputSpeakerButton.tintColor = ThemeManager.currentTheme().controlButtonsColor
        clearButton.tintColor = ThemeManager.currentTheme().controlButtonsColor
        outputSpeakerButton.tintColor = ThemeManager.currentTheme().controlButtonsColor
        bookmarkButton.tintColor = ThemeManager.currentTheme().controlButtonsColor
        expandButton.tintColor = ThemeManager.currentTheme().controlButtonsColor
        shareButton.tintColor = ThemeManager.currentTheme().controlButtonsColor
        copyButton.tintColor = ThemeManager.currentTheme().controlButtonsColor
        
        clipboardNotificationView.isHidden = true
        
        addButtonOnKeyboard(ofTextView: inputTextView, withHeight: 64, title: "SCRMBL", titleColor: ThemeManager.currentTheme().notificationViewColor, backgroundColor: ThemeManager.currentTheme().subtitleTextColor, action: #selector(keyboardButtonPressed))
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler(_:)))
        view.addGestureRecognizer(tapGesture)
        
        let outputTapGesture = UITapGestureRecognizer(target: self, action: #selector(outputTapGestureHandler(_:)))
        outputTextView.addGestureRecognizer(outputTapGesture)

        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressGestureHandler(_:)))
        longPressGestureRecognizer.minimumPressDuration = 1.0
        outputTextView.addGestureRecognizer(longPressGestureRecognizer)
        chosenLevel = AppSettings.shared.getChosenLevel()
        
        if AppSettings.shared.getScrambleFromClipboardSettings() {
            if AppSettings.shared.getTextFromClipboard() != "", !AppSettings.shared.getTextFromClipboard().isEmpty {
                clipboardNotificationView.isHidden = false
                clipboardNotificationView.alpha = 1.0
                Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateClipboardNotificationView), userInfo: nil, repeats: false)
                
            }
        }
        
    }
    
    private func addButtonOnKeyboard(ofTextView textView: UITextView, withHeight height: CGFloat, title: String, titleColor: UIColor, backgroundColor: UIColor, action: Selector) {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: height))
        
        let button = UIButton(frame: customView.bounds)
        button.setTitle(title, for: .normal)
        button.addTarget(self, action: action, for: .touchUpInside)
        button.setTitleColor(titleColor, for: .normal)
        button.backgroundColor = backgroundColor
        
        customView.addSubview(button)
        
        textView.inputAccessoryView = customView
    }
    
    private func synthesizeSpeech(from string: String) {
        let speechUtterane = AVSpeechUtterance(string: string)
        speechSynthesizer.speak(speechUtterane)
    }
    
    private func popNotification(withText text: String) {
        notificationMessageLabel.text = text
        notificationView.isHidden = false
        notificationView.alpha = 1.0
        
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCopyNotificationView), userInfo: nil, repeats: false)
    }
    
    //MARK: - Core Data
    private func save(completion: CompletionHandler) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        let  bookmark = Bookmark(context: managedContext)
        
        if inputTextView.text != nil && inputTextView.text != "" && outputTextView.text != nil && outputTextView.text != "" {
            bookmark.oldText = inputTextView.text!
            bookmark.scrambledText = outputTextView.text!
            bookmark.saveStatus = true
        }
        
        do {
            try managedContext.save()
            print("Successfully saved in CoreData.")
            completion(true)
        }catch {
            debugPrint("Could not save \(error.localizedDescription)")
            completion(false)
        }
    }
    
    func bookmarkAlreadyExists(in context: NSManagedObjectContext) -> Bool {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Bookmark")
        fetchRequest.includesSubentities = false
        var entitiesCount = 0
        do {
            entitiesCount = try context.count(for: fetchRequest)
        }
        catch {
            print("error executing fetch request: \(error)")
        }
        return entitiesCount > 0
    }
    
    //MARK: - Handlers
    @objc func longPressGestureHandler(_ gesture: UIGestureRecognizer) {
        switch gesture.state {
        case .ended:
            if outputTextView.text != nil && outputTextView.text != "" {
                UIPasteboard.general.string = outputTextView.text
                popNotification(withText: "Text Copied")
            }
        default: break
        }
    }
    
    @objc func outputTapGestureHandler(_ gesture: UITapGestureRecognizer) {
        switch gesture.state {
        case .ended:
            if outputTextView.text != nil && outputTextView.text != "" {
                outputTextView.text = Scramble().getScrambled(text: inputTextView.text, forLevel: chosenLevel!)
                popNotification(withText: "Scrambled")
            }
        default: break
        }
    }
    
    @objc func keyboardButtonPressed() {
        print("Keyboard button pressed.")
        if inputTextView.text != nil && inputTextView.text != "" {
            //TODO: - Model here
            outputTextView.text = Scramble().getScrambled(text: inputTextView.text, forLevel: chosenLevel!)
            outputControls.isHidden = false
            
        }else {
            outputTextView.text = nil
            outputControls.isHidden = true
        }
        inputTextView.resignFirstResponder()
    }
    
    @objc func tapGestureHandler(_ gesture: UITapGestureRecognizer) {
        inputTextView.resignFirstResponder()
    }
    
    @objc private func updateCopyNotificationView() {
        UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseIn, animations: {
            self.notificationView.alpha = 0.0
        }) { (complete) in
            if complete {
                self.notificationView.isHidden = true
            }
        }
    }
    
    @objc private func updateClipboardNotificationView() {
        UIView.animate(withDuration: 1.0, delay: 4, options: .curveEaseIn, animations: {
            self.clipboardNotificationView.alpha = 0.0
        }) { (complete) in
            if complete {
                self.clipboardNotificationView.isHidden = true
            }
        }
    }
    
    //MARK: - Actions
    @IBAction func languageButtonPressed(_ sender: UIButton) {
        guard let languagesVC = storyboard?.instantiateViewController(withIdentifier: StoryboardIDs.LanguagesViewControllerId) as? LevelsViewController else { return }
        present(languagesVC, animated: true, completion: nil)
    }
    
    @IBAction func clearButtonPressed(_ sender: UIButton) {
        if inputTextView.text != "Write something here..." {
            inputTextView.textColor = UIColor.lightGray
            inputTextView.text = "Write something here..."
            inputTextView.resignFirstResponder()
            outputTextView.text = nil
            outputControls.isHidden = true
            clearButton.isHidden = true
        }
    }
    
    @IBAction func inputSpeakerButtonPressed(_ sender: UIButton) {
        if inputTextView.text != nil && inputTextView.text != "" {
            inputSpeakerButton.tintColor = ThemeManager.currentTheme().mainColor
            synthesizeSpeech(from: inputTextView.text)
        }
    }
    
    @IBAction func outputSpeakerButtonPressed(_ sender: UIButton) {
        if outputTextView.text != nil && outputTextView.text != "" {
            outputSpeakerButton.tintColor = ThemeManager.currentTheme().mainColor
            synthesizeSpeech(from: outputTextView.text)
        }
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        if inputTextView.text != nil && inputTextView.text != "" && outputTextView.text != nil && outputTextView.text != "" {
            if !savedTexts.contains(outputTextView.text!) {
                savedTexts.append(outputTextView.text)
                save { success in
                    if success {
                        popNotification(withText: "Saved")
                    }
                }
            }else {
                popNotification(withText: "Already Saved")
            }
        }
    }
    
    @IBAction func shareButtonPressed(_ sender: UIButton) {
        if outputTextView.text != nil && outputTextView.text != "" {
            let textToShare = outputTextView.text!
            let objectsToShare = [textToShare]
            
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.excludedActivityTypes = [.assignToContact]
            
            activityVC.popoverPresentationController?.sourceView = sender
            present(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func expandButtonPressed(_ sender: UIButton) {
        if outputTextView.text != nil && outputTextView.text != "" {
            guard let expandVC = storyboard?.instantiateViewController(withIdentifier: StoryboardIDs.ExpandViewControllerId) as? ExpandViewController else { return }
            expandVC.initWith(text: outputTextView.text!)
            present(expandVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func copyButtonPressed(_ sender: UIButton) {
        if outputTextView.text != nil && outputTextView.text != "" {
            UIPasteboard.general.string = outputTextView.text
            popNotification(withText: "Text Copied")
        }
    }
    
    @IBAction func scrambleFromClipboardButtonPressed(_ sender: UIButton) {
        clipboardNotificationView.isHidden = true
        inputTextView.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        inputTextView.text = AppSettings.shared.getTextFromClipboard()
        
        //TODO: - Model Here
        outputTextView.text = Scramble().getScrambled(text: inputTextView.text, forLevel: self.chosenLevel!)
        outputControls.isHidden = false
        clearButton.isHidden = false
        print("Copied From Clipboard")
    }
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) { }
    
}

//MARK: - Delegates
extension ScrambleViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Write something here..." {
            textView.text = ""
            textView.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            clearButton.isHidden = false
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" || textView.text.isEmpty || textView.text == nil {
            textView.textColor = UIColor.lightGray
            textView.text = "Write something here..."
            clearButton.isHidden = true
        }else {
            //TODO: - Model here
            outputTextView.text = Scramble().getScrambled(text: inputTextView.text, forLevel: chosenLevel!)
            outputControls.isHidden = false
            clearButton.isHidden = false
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if AppSettings.shared.getScrambleOnTapReturn() {
            textView.returnKeyType = .default
            if (text == "\n") {
                textView.resignFirstResponder()
                print("shouldChangeTextIn - true")
                return false
            }
            return true
        }
        textView.returnKeyType = .done
        return true
    }
    
}

extension ScrambleViewController: AVSpeechSynthesizerDelegate {
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didStart utterance: AVSpeechUtterance) {
        inputTextView.isUserInteractionEnabled = false
        inputSpeakerButton.isUserInteractionEnabled = false

        outputSpeakerButton.isUserInteractionEnabled = false
        outputSpeakerButton.isUserInteractionEnabled = false
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        inputTextView.isUserInteractionEnabled = true
        inputSpeakerButton.isUserInteractionEnabled = true

        outputSpeakerButton.isUserInteractionEnabled = true
        outputSpeakerButton.isUserInteractionEnabled = true
        
        inputSpeakerButton.tintColor = ThemeManager.currentTheme().controlButtonsColor
        outputSpeakerButton.tintColor = ThemeManager.currentTheme().controlButtonsColor
    }
}

