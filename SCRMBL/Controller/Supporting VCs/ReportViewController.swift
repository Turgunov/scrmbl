//
//  ReportViewController.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 16.07.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit
import MessageUI

class ReportViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var navigationBarTitleLabel: UILabel!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var sendButton: RoundedButton!
    
    var mail: MFMailComposeViewController?
    
    //MARK: - ViewControllerLifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        messageTextView.delegate = self
        mail?.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareTheme()
    }
    
    //MARK: - Utility Functions
    private func prepareTheme() {
        backButton.setImage(#imageLiteral(resourceName: "close").withRenderingMode(.alwaysTemplate), for: .normal)
        backButton.tintColor = ThemeManager.currentTheme().titleTextColor
        messageView.layer.borderColor = ThemeManager.currentTheme().mainColor.cgColor
        navigationBarView.backgroundColor = ThemeManager.currentTheme().mainColor
        navigationBarTitleLabel.textColor = ThemeManager.currentTheme().titleTextColor
        sendButton.backgroundColor = ThemeManager.currentTheme().keyboardAccessoryViewColor
        sendButton.setAttributedTitle(NSAttributedString(string: (sendButton.titleLabel?.text)!, attributes: [.foregroundColor : ThemeManager.currentTheme().subtitleTextColor]), for: .normal)
    }
    
    private func sendEmail() {
        if emailTextField.text != "" && emailTextField.text != "Your email" {
            if let body = messageTextView.text {
                mail = MFMailComposeViewController()
                mail?.mailComposeDelegate = self
                mail?.setToRecipients(reportRecipients)
                mail?.setSubject("Feedback")
                mail?.setMessageBody(body, isHTML: false)
                guard let mailVC = mail else { return }
                present(mailVC, animated: true, completion: nil)
            }
            
        }else {
            popEmailEntryNotification()
        }
    }
    
    private func popEmailEntryNotification() {
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCopyNotificationView), userInfo: nil, repeats: false)
    }
    
    //MARK: - Handlers
    @objc private func updateCopyNotificationView() {
        UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseIn, animations: {
            self.emailTextField.alpha = 0.0
        }) { (complete) in
            if complete {
                UIView.animate(withDuration: 0.5) {
                    self.emailTextField.alpha = 1
                }
            }
        }
    }
    
    //MARK: - Actions
    @IBAction func backButtonPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendButtonPressed(_ sender: UIButton) {
        sendEmail()
    }
    
}

//MARK: - Extensions
extension ReportViewController: MFMailComposeViewControllerDelegate, UINavigationControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension ReportViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Write your feedback here..." {
            textView.text = ""
            textView.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" || textView.text.isEmpty || textView.text == nil {
            textView.textColor = UIColor.lightGray
            textView.text = "Write your feedback here..."
        }
    }
}
