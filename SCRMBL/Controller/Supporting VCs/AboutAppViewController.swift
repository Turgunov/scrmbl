//
//  AboutAppViewController.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 16.07.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit

class AboutAppViewController: UIViewController {
    

    //MARK: - Outlets
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var navigationBarTitleLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareTheme()
    }
    
    //MARK: - Utility Functions
    private func prepareTheme() {
        backButton.setImage(#imageLiteral(resourceName: "close").withRenderingMode(.alwaysTemplate), for: .normal)
        backButton.tintColor = ThemeManager.currentTheme().titleTextColor
        navigationBarView.backgroundColor = ThemeManager.currentTheme().mainColor
        navigationBarTitleLabel.textColor = ThemeManager.currentTheme().titleTextColor
    }
    
    //MARK: - Actions
    @IBAction func backButtonPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

}
