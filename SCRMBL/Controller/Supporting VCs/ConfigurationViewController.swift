//
//  ConfigurationViewController.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 24.09.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit

class ConfigurationViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!

    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var navigationBarTitleLabel: UILabel!
    
    @IBOutlet weak var ignoredCharactersTextView: UITextView!
    @IBOutlet weak var ignoreNumbersStatusSwitch: UISwitch!

    @IBOutlet weak var defaultButton: RoundedButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        ignoredCharactersTextView.delegate = self
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler(_:)))
        view.addGestureRecognizer(tapGesture)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareTheme()
        ignoredCharactersTextView.text = AppSettings.shared.getSymbolLibraryString()
    }
    
    //MARK: - Utility Functions
    private func prepareTheme() {
        backButton.setImage(#imageLiteral(resourceName: "close").withRenderingMode(.alwaysTemplate), for: .normal)
        backButton.tintColor = ThemeManager.currentTheme().navigationButtonsColor
        
        saveButton.setTitleColor(ThemeManager.currentTheme().navigationButtonsColor, for: .normal)
        
        navigationBarView.backgroundColor = ThemeManager.currentTheme().mainColor
        navigationBarTitleLabel.textColor = ThemeManager.currentTheme().titleTextColor
        
        defaultButton.backgroundColor = ThemeManager.currentTheme().keyboardAccessoryViewColor
        defaultButton.setAttributedTitle(NSAttributedString(string: (defaultButton.titleLabel?.text)!, attributes: [.foregroundColor : ThemeManager.currentTheme().titleTextColor]), for: .normal)
    }
    
    //MARK: - Handlers
    @objc func tapGestureHandler(_ gesture: UITapGestureRecognizer) {
        ignoredCharactersTextView.resignFirstResponder()
    }
    
    //MARK: - Actions
    @IBAction func backButtonPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func defaultButtonPressed(_ sender: UIButton) {
        AppSettings.shared.symbolLibrary = "~!@#$%^&*()|_+{}:?<>"
        AppSettings.shared.ignoreNumbersStatus = true
        ignoreNumbersStatusSwitch.isOn = true
        ignoredCharactersTextView.text = "~!@#$%^&*()|_+{}:?<>"
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        if ignoredCharactersTextView.text != "Warning! All symbols are allowed." || ignoredCharactersTextView.text != " " {
            AppSettings.shared.symbolLibrary = ignoredCharactersTextView.text
        }
        AppSettings.shared.ignoreNumbersStatus = ignoreNumbersStatusSwitch.isOn
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ignoreNumbersStatusChanged(_ sender: UISwitch) {
        AppSettings.shared.ignoreNumbersStatus = sender.isOn ? true : false
    }
    
}

//MARK: - Extensions
extension ConfigurationViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "~!@#$%^&*()|_+{}:?<>" {
            textView.text = "~!@#$%^&*()|_+{}:?<>"
            textView.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }else if textView.text == "Warning! All symbols are allowed." {
            textView.text = ""
            textView.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }else {
            textView.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" || textView.text.isEmpty || textView.text == nil {
            textView.textColor = UIColor.lightGray
            textView.text = "Warning! All symbols are allowed."
        }else if textView.text == "~!@#$%^&*()|_+{}:?<>" {
            textView.textColor = UIColor.lightGray
        }
    }
}
