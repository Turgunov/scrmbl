//
//  ExpandViewController.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 17.07.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit

class ExpandViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var scrambledTextLabel: UILabel!
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var notificationMessageLabel: UILabel!
    
    var text: String?
    
    //MARK: - ViewControllerLifecycle
    func initWith(text: String) {
        self.text = text
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrambledTextLabel.text = text
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareTheme()
    }
    
    //MARK: - Utility Functions
    @objc private func prepareTheme() {
        closeButton.setImage(#imageLiteral(resourceName: "close").withRenderingMode(.alwaysTemplate), for: .normal)
        closeButton.tintColor = ThemeManager.currentTheme().titleTextColor
        backgroundView.backgroundColor = ThemeManager.currentTheme().mainColor
        scrambledTextLabel.textColor = ThemeManager.currentTheme().titleTextColor
        notificationView.backgroundColor = ThemeManager.currentTheme().notificationViewColor
        notificationMessageLabel.textColor = ThemeManager.currentTheme().subtitleTextColor
    }
    
    private func setUpView() {
        let longPressGestureRecognizerForOriginal = UILongPressGestureRecognizer(target: self, action: #selector(longPressForOriginalTextHandler(_:)))
        scrambledTextLabel.isUserInteractionEnabled = true
        longPressGestureRecognizerForOriginal.minimumPressDuration = 1.0
        scrambledTextLabel.addGestureRecognizer(longPressGestureRecognizerForOriginal)
    }
    
    private func popNotification(withText text: String) {
        notificationMessageLabel.textColor = backgroundView.backgroundColor
        notificationMessageLabel.text = text
        notificationView.isHidden = false
        notificationView.alpha = 1.0
        
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCopyNotificationView), userInfo: nil, repeats: false)
    }
    
    //MARK: - Handlers
    @objc func longPressForOriginalTextHandler(_ gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .ended:
            if scrambledTextLabel.text != nil && scrambledTextLabel.text != "" {
                UIPasteboard.general.string = scrambledTextLabel.text
                popNotification(withText: "Text Copied")
            }
        default: break
        }
    }
    
    @objc private func updateCopyNotificationView() {
        UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseIn, animations: {
            self.notificationView.alpha = 0.0
        }) { (complete) in
            if complete {
                self.notificationView.isHidden = true
            }
        }
    }
    
    //MARK: - Actions
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}
