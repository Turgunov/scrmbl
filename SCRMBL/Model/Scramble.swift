//
//  Scramble.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 16.07.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

enum Level: Int {
    case veryLow = 10, low = 8, medium = 6, high = 4, veryHigh = 2
    
    func getSelectedLanguage() -> String {
        switch self {
        case .veryLow: return "Very Low"
        case .low: return "Low"
        case .medium: return "Medium"
        case .high: return "High"
        case .veryHigh: return "Very High"
        }
    }
}

import Foundation

struct Scramble {
    
    let alphanumericLibrary = CharacterSet.alphanumerics

    let numbersLibrary = Array("0123456789")
    let symbolLibrary = AppSettings.shared.getSymbolLibrary()
    
    func getScrambled(text: String, forLevel level: Level) -> String {
        let textArray = text.components(separatedBy: " ")
        
        var scrambledTextArray: [String] = []
        
        textArray.forEach { (word) in
            var scrambledWord = ""
            
            //Check for numbers
            var nonAlphabeticCharacterIndexes = [[String.Index:Character]]()
            var wordLocal = word
            
            if AppSettings.shared.getIgnoreNumbersStatus() {
                wordLocal.forEach { (character) in
                    for number in numbersLibrary {
                        if "\(character)".contains(number) {
                            if let index = wordLocal.range(of: "\(number)")?.lowerBound {
                                nonAlphabeticCharacterIndexes.append([index : wordLocal.remove(at: index)])
                            }
                        }
                    }
                }
            }

            wordLocal.forEach({ (character) in
                for symbol in symbolLibrary {
                    if "\(character)".contains(symbol) {
                        if let index = wordLocal.range(of: "\(symbol)")?.lowerBound {
                            nonAlphabeticCharacterIndexes.append([index : wordLocal.remove(at: index)])
                        }
                    }
                    
                }
            })
            
            if wordLocal.count > (level.rawValue + 1) {
                let prefixCount = level.rawValue / 2
                let suffixCount = level.rawValue / 2
                
                let prefix = wordLocal.prefix(prefixCount).map { $0 }
                let suffix = wordLocal.suffix(suffixCount).map { $0 }
                
                var middlePart = wordLocal.dropFirst(prefixCount).dropLast(suffixCount).map { $0 }.shuffled()
                middlePart.insert(contentsOf: prefix, at: 0)
                middlePart.insert(contentsOf: suffix, at: middlePart.endIndex)
                
                middlePart.forEach { (character) in
                    scrambledWord += String.init(character)
                }
                
                nonAlphabeticCharacterIndexes.reverse()
                nonAlphabeticCharacterIndexes.forEach({ (dictionary) in
                    for (key, value) in dictionary {
                        scrambledWord.insert(value, at: key)
                    }
                })
                nonAlphabeticCharacterIndexes.removeAll()
            }else {
                scrambledWord += wordLocal
            }
            scrambledTextArray.append(scrambledWord)
        }
        
        let finishedText = scrambledTextArray.joined(separator: " ")
        
        return finishedText
    }
    
}

extension StringProtocol where Index == String.Index {
    func encodedOffset(of element: Element) -> Int? {
        return index(of: element)?.encodedOffset
    }
    func encodedOffset(of string: String) -> Int? {
        return range(of: string)?.lowerBound.encodedOffset
    }
}
