//
//  AppSettings.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 18.07.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import Foundation

class AppSettings: WRUserSettings {
    
    //MARK: - Public variables
    dynamic var shouldScrambleFromClipboard: Bool = false
    dynamic var textFromClipboard: String = ""
    dynamic var shouldScrambleOnTapReturn: Bool = false
    dynamic var selectedColorButtonIndex: Int = 1
    dynamic var chosenLevel: String = "Very High"
    dynamic var selectedTheme: String = "yellow"
    dynamic var themeDidChange: Bool = false
    dynamic var symbolLibrary = "~!@#$%^&*()|_+{}:?<>"
    dynamic var ignoreNumbersStatus: Bool = true

    //MARK: - Getters
    func getScrambleFromClipboardSettings() -> Bool {
        return shouldScrambleFromClipboard
    }
    
    func getTextFromClipboard() -> String {
        return textFromClipboard
    }
    
    func getScrambleOnTapReturn() -> Bool {
        return shouldScrambleOnTapReturn
    }
    
    func getSelectedColorButtonIndex() -> Int {
        return selectedColorButtonIndex
    }
    
    func getChosenLevel() -> Level {
        switch chosenLevel {
        case "Very High":
            return Level.veryHigh
        case "High":
            return Level.high
        case "Medium":
            return Level.medium
        case "Low":
            return Level.low
        case "Very Low":
            return Level.veryLow
        default:
            return Level.veryHigh
        }
    }

    func getChosenLevelName() -> String {
        return chosenLevel
    }
    
    func getSelectedTheme() -> Theme {
        switch selectedTheme {
        case "green": return .green
        case "yellow": return .yellow
        case "blue": return .blue
        case "navy": return .navy
        case "dark": return .dark
        case "red": return .red
        default: return .yellow
        }
    }
    
    func getSymbolLibrary() -> [Character] {
        return Array(symbolLibrary)
    }
    
    func getSymbolLibraryString() -> String {
        return symbolLibrary
    }
    
    func getIgnoreNumbersStatus() -> Bool {
        return ignoreNumbersStatus
    }
}

