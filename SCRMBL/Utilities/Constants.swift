//
//  Constants.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 14.07.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

typealias CompletionHandler = (_ success: Bool) -> ()

let appDelegate = UIApplication.shared.delegate as? AppDelegate

import Foundation
import UIKit

enum Theme: Int {
    
    case green, yellow, blue, navy, dark, red
    
    var mainColor: UIColor {
        switch self {
        case .green:
            return UIColor().colorFromHexString("8FCF4E")
        case .yellow:
            return UIColor().colorFromHexString("FFFB00")
        case .blue:
            return UIColor().colorFromHexString("0096FF")
        case .navy:
            return UIColor().colorFromHexString("#6A7086")
        case .dark:
            return UIColor().colorFromHexString("414141")
        case .red:
            return UIColor().colorFromHexString("C22C2E")
        }
    }
    
    var notificationViewColor: UIColor {
        switch self {
        case .green:
            return UIColor().colorFromHexString("555555")
        case .yellow:
            return UIColor().colorFromHexString("555555")
        case .blue:
            return UIColor().colorFromHexString("555555")
        case .navy:
            return UIColor().colorFromHexString("555555")
        case .dark:
            return UIColor().colorFromHexString("555555")
        case .red:
            return UIColor().colorFromHexString("555555")
        }
    }
    
    var titleTextColor: UIColor {
        switch self {
        case .green:
            return UIColor().colorFromHexString("FFFFFF")
        case .yellow:
            return UIColor().colorFromHexString("000000")
        case .blue:
            return UIColor().colorFromHexString("FFFFFF")
        case .navy:
            return UIColor().colorFromHexString("FFFFFF")
        case .dark:
            return UIColor().colorFromHexString("FFFFFF")
        case .red:
            return UIColor().colorFromHexString("FFFFFF")
        }
    }
    
    var subtitleTextColor: UIColor {
        switch self {
        case .green:
            return UIColor().colorFromHexString("FFFFFF")
        case .yellow:
            return UIColor().colorFromHexString("FFFFFF")
        case .blue:
            return UIColor().colorFromHexString("FFFFFF")
        case .navy:
            return UIColor().colorFromHexString("FFFFFF")
        case .dark:
            return UIColor().colorFromHexString("FFFFFF")
        case .red:
            return UIColor().colorFromHexString("FFFFFF")
        }
    }
    
    var keyboardAccessoryViewColor: UIColor {
        switch self {
        case .green:
            return UIColor().colorFromHexString("8FCF4E")
        case .yellow:
            return UIColor().colorFromHexString("FFFB00")
        case .blue:
            return UIColor().colorFromHexString("0096FF")
        case .navy:
            return UIColor().colorFromHexString("6A7086")
        case .dark:
            return UIColor().colorFromHexString("414141")
        case .red:
            return UIColor().colorFromHexString("C22C2E")
        }
    }
    
    var keyboardAccessoryTextColor: UIColor {
        switch self {
        case .green:
            return UIColor().colorFromHexString("FFFFFF")
        case .yellow:
            return UIColor().colorFromHexString("000000")
        case .blue:
            return UIColor().colorFromHexString("FFFFFF")
        case .navy:
            return UIColor().colorFromHexString("FFFFFF")
        case .dark:
            return UIColor().colorFromHexString("FFFFFF")
        case .red:
            return UIColor().colorFromHexString("FFFFFF")
        }
    }
    
    var navigationButtonsColor: UIColor {
        switch self {
        case .green:
            return UIColor.white
        case .yellow:
            return UIColor.darkGray
        case .blue:
            return UIColor.white
        case .navy:
            return UIColor.white
        case .dark:
            return UIColor.white
        case .red:
            return UIColor.white
        }
    }
    
    var controlButtonsColor: UIColor {
        switch self {
        case .green:
            return UIColor.darkGray
        case .yellow:
            return UIColor.darkGray
        case .blue:
            return UIColor.darkGray
        case .navy:
            return UIColor.darkGray
        case .dark:
            return UIColor.darkGray
        case .red:
            return UIColor.darkGray
        }
    }
    
    var clipboardNotificationSubtitleColor: UIColor {
        switch self {
        case .green:
            return UIColor().colorFromHexString("8FCF4E")
        case .yellow:
            return UIColor().colorFromHexString("FFFB00")
        case .blue:
            return UIColor().colorFromHexString("0096FF")
        case .navy:
            return UIColor().colorFromHexString("#D3D3D3")
        case .dark:
            return UIColor().colorFromHexString("FFFFFF")
        case .red:
            return UIColor().colorFromHexString("C22C2E")
        }
    }
    
}

let reportRecipients = ["o.r.turgunov@gmail.com"]

struct Cells {
    static let BookmarkCellId = "BookmarkCell"
}

struct Segues {
    static let ToLanguagesVC = "toLanguagesVC"
}

struct StoryboardIDs {
    static let ReportViewControllerId = "ReportViewController"
    static let AboutAppViewControllerId = "AboutAppViewController"
    static let ConfigurationViewControllerId = "ConfigurationViewController"
    static let ExpandViewControllerId = "ExpandViewController"
    static let ExpandBookmarkViewControllerId = "ExpandBookmarkViewController"
    static let LanguagesViewControllerId = "LevelsViewController"
}

struct Observers {
    static let themeDidChangeKey = "applicationThemeValueChangedNotification"
}

