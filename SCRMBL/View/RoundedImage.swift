//
//  RoundedImageView.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 12.09.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedImage: UIImageView {

    @IBInspectable var cornerRadius: CGFloat = 25
    
    //MARK: - ViewControllerLifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = cornerRadius
    }

}
