//
//  BookmarkCell.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 17.07.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit

class BookmarkCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var oldTextLabel: UILabel!
    @IBOutlet weak var scrambledTextLabel: UILabel!
    @IBOutlet weak var starButton: UIButton!
    @IBOutlet weak var roundedView: RoundedView!
    
    weak var delegate: BookmarkCellDelegate?

    //MARK: - Actions
    @IBAction func starButtonTapped(_ sender: UIButton) {
        if sender.imageView?.image == #imageLiteral(resourceName: "starFilled") {
            sender.setImage(#imageLiteral(resourceName: "star"), for: .normal)
            delegate?.bookmarkCellDidTapStar(self)
        }else {
            sender.setImage(#imageLiteral(resourceName: "starFilled"), for: .normal)
            delegate?.bookmarkCellDidTapStarFilled(self)
        }
    }
    
    //MARK: - Utility Functions
    func configureCell(bookmark: Bookmark) {
        roundedView.layer.borderColor = ThemeManager.currentTheme().mainColor.cgColor
        oldTextLabel.text = bookmark.oldText
        scrambledTextLabel.text = bookmark.scrambledText
        
        if bookmark.saveStatus {
            starButton.setImage(#imageLiteral(resourceName: "starFilled"), for: .normal)
        }else {
            starButton.setImage(#imageLiteral(resourceName: "star"), for: .normal)
        }
    }
    
}

protocol BookmarkCellDelegate : class {
    func bookmarkCellDidTapStar(_ sender: BookmarkCell)
    func bookmarkCellDidTapStarFilled(_ sender: BookmarkCell)
}


