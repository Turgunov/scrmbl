//
//  CopyFromClipboardNotificationView.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 19.09.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit

class CopyFromClipboardNotificationView: UIView {

    @IBOutlet weak var titleTextLabel: UILabel!
    @IBOutlet weak var subtitleTextLabel: UILabel!

}
