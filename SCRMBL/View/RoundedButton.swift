//
//  RoundedButton.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 14.07.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedButton: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 10

    //MARK: - ViewControllerLifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = cornerRadius
    }

}
