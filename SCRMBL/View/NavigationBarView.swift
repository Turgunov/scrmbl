//
//  NavigationBarView.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 23.08.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit

class NavigationBarView: UIView {

    //MARK: - ViewControllerLifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.backgroundColor = ThemeManager.currentTheme().mainColor.cgColor
    }
}
