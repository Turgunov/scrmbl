//
//  CircledButton.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 14.07.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import UIKit

class CircledButton: UIButton {
    
    var selectionStatus: Bool = false
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
    
    //MARK: - ViewControllerLifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = self.layer.frame.height / 2
        layer.borderColor = #colorLiteral(red: 0.7540688515, green: 0.7540867925, blue: 0.7540771365, alpha: 1)
    }
    
    override func setNeedsLayout() {
        super.setNeedsLayout()
        if selectionStatus {
            layer.borderWidth = 3.0
        }else {
            layer.borderWidth = 0
        }
    }
    
    
}
