//
//  ArrayExtention.swift
//  SCRMBL
//
//  Created by Olimjon Turgunov on 17.07.18.
//  Copyright © 2018 Olimjon Turgunov. All rights reserved.
//

import Foundation

extension Array {
    
    func filterDuplicates(includeElement: @escaping (_ lhs:Element, _ rhs:Element) -> Bool) -> [Element]{
        var results = [Element]()
        forEach { (element) in
            let existingElements = results.filter {
                return includeElement(element, $0)
            }
            if existingElements.count == 0 {
                results.append(element)
            }
        }
        return results
    }
    
}
