//
//  SCRMBLUITests.swift
//  SCRMBLUITests
//
//  Created by Olimjon Turgunov on 06.02.19.
//  Copyright © 2019 Olimjon Turgunov. All rights reserved.
//

import XCTest

class SCRMBLUITests: XCTestCase {

    let app = XCUIApplication()

    
    override func setUp() {
        super.setUp()
        
        app.launchArguments = ["UITesting"]
        app.launch()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

}
